package com.worldpay.offergoods.model.constants;

public enum Category {
	BOOK, GAME_CONSOLE, HOME_ELECTRONICS
}
