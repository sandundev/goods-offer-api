package com.worldpay.offergoods;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
/**
 * 
 * 
 * @author Sandun Lewke Bandara
 */

@SpringBootApplication
@EnableAutoConfiguration
//@ComponentScan("com.worldpay.offergoods")
//@EntityScan("com.worldpay.offergoods.model")
//@EnableJpaRepositories("com.worldpay.offergoods.repository")
@PropertySource("classpath:application.properties")
public class GoodsOfferRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoodsOfferRestApplication.class, args);
	}
}
