package com.worldpay.offergoods.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.worldpay.offergoods.model.MerchantAccount;

@Repository
public interface MerchantAccountRepository extends CrudRepository<MerchantAccount, Long> {

}